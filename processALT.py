""" processALT.py
#
# Brian Tremaine 6/21/2018
# script to read in and process the CVS files from the laser test racks (ALT)
#
"""

import os
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

# import time
import logging, sys
import argparse
import csv

print(" commented out scikit-learn funcs")

""" ============ main ======================================
"""
if __name__ == '__main__':

    logging.basicConfig(filename='alt.log1', filemode='w', level=logging.DEBUG)
    logging.info("Program started")

    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--file", nargs='*', required=True,
                    help="path to input *.log csv file")
    args = vars(ap.parse_args())

    # Load the CSV file for processing (ext is *.log)
    # Row 1:     Comment
    # Rows 2:... Data
    # Data columns:
    # Time_stamp, hours, L0, L1, L2, ..., L19, Vref, Gnd
    # L0---Gnd data is in ADC counts

    data = []
    TimeStamp = list()
    hours = list()
    data = list()

    N = 24
    files = args['file']
    for file in files:
        hours = []
        with open(file, newline='') as csvfile:
            print("Headers found:")
            filereader = csv.reader(csvfile, delimiter=',')  # , quotechar='|')
            line = 0
            d0 = 0
            for row in filereader:
                if line == 0:
                    header = row
                    print(row)  # comment field
                else:
                    TimeStamp.append(row[0])
                    hours.append(row[1])
                    data = row[2:(N)]
                    d1 = np.asarray(data)
                    if line == 1:
                        d = d1
                    else:
                        d = np.vstack([d0, d1])
                    d0 = d

                line = line + 1
                # scale factor, volts/bit
        scale = 2.50 / 1024.0

        # convert str to float
        Gnd = scale * np.asarray(d[:, N - 3]).astype(float)
        Vref = scale * np.asarray(d[:, N - 4]).astype(float)
        hours = np.asarray(hours).astype(float)

        laser = d[:, 0:20]
        laser = scale * np.asarray(laser).astype(float)

        #
        xtitle = 'hours:  ' + file

        # plot results for Vref & Gnd in one plot

        plt.figure(1)
        plt.title(header[0] + ': Vref and Gnd')
        plt.xlabel(xtitle)
        plt.ylabel('volts')
        plt.plot(hours, Gnd)
        plt.plot(hours, Vref)
        #
        plt.show()

        # process laser data absolute scaled and normalized to time 0

        plt.figure(2)
        plt.title(header[0] + ': laser absolute')
        plt.xlabel(xtitle)
        plt.ylabel('volts')
        plt.axis([0, hours[-1] - hours[0], 0.0, 1.0])
        plt.plot(hours - hours[0], laser, '.')
        #
        plt.show()

        eps = 0.010
        laserS = laser.copy()
        Nstart = 400
        for i in range(0, 20):
            if laser[Nstart, i] > eps:
                laserS[:, i] = laser[:, i] / laser[Nstart, i]

        plt.figure(3)
        plt.title(header[0] + ': laser normalized to 100hrs, full-range')
        plt.xlabel(xtitle)
        plt.ylabel('ratio')
        plt.axis([0, hours[-1] - hours[0], 0.0, 1.1])
        plt.plot(hours - hours[0], laserS, '.')

        plt.figure(4)
        plt.title(header[0] + ': laser normalized to 100hrs, zoom y-axis')
        plt.xlabel(xtitle)
        plt.ylabel('ratio')
        plt.axis([0, hours[-1] - hours[0], 0.75, 1.1])
        plt.plot(hours - hours[0], laserS, '.')

        # extrapolate time for 30% degradation
        # Train the model using the training sets
        # Create linear regression object
        regr = linear_model.LinearRegression()

        Nstart = 400
        print('Start time = ' + str(hours[Nstart]))
        print('Coefficients: \n')
        for i in range(0, 20):
            regr.fit(hours[Nstart:].reshape(-1, 1), laserS[Nstart:, i].reshape(-1, 1))
            #
            # extrapolate 50%
            if (regr.coef_[0, 0] < 0):
                x = -0.30 / regr.coef_[0, 0]
            else:
                x = 0
            print(i, int(10 * x) / 10.0)
